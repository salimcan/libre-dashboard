import { useMemo } from "react";
import { liquidityTokenPairs } from "../models/Tokens";
import useFarmingStats from "./queries/farming/useFarmingStats";
import useTokenInformation from "./queries/tokens/useTokenInformation";
import { IExchangeRates, useExchangeRates } from "./useExchangeRates";

export const usePoolsTableData = () => {
  const { data: poolStats, isLoading: isLoadingPoolStats } = useFarmingStats();
  const { data: tokenData, isLoading: isLoadingTokenData } =
    useTokenInformation();
  const { data: exchangeRates, isLoading: isLoadingExchangeRates } =
    useExchangeRates();

  const data = useMemo(() => {
    let builtData: any[] = [];
    if (!poolStats || !tokenData || !exchangeRates) return builtData;
    builtData = poolStats.map((ps) => {
      const token = tokenData.find(
        (td) => td.supply.split(" ")[1] === ps.symbol
      );
      if (!token) return;
      return {
        pairs: liquidityTokenPairs[process.env.NEXT_PUBLIC_ENV][ps.symbol],
        apy: `${Number(ps.apy).toFixed(2)}%`,
        total_liquidity_pool: `$${Number(
          Number(
            Number(token.supply.split(" ")[0]) *
              exchangeRates[ps.symbol as keyof IExchangeRates]
          ).toFixed(2)
        ).toLocaleString("en-US")}`,
        farmed_percentage: `${Number(
          (Number(ps.farming_staked) / Number(token.supply.split(" ")[0])) * 100
        ).toFixed(2)}%`,
      };
    });
    return builtData;
  }, [poolStats, tokenData, exchangeRates]);

  return {
    data,
    isLoading:
      isLoadingExchangeRates || isLoadingTokenData || isLoadingPoolStats,
  };
};
