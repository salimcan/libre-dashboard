import { validate } from "bitcoin-address-validation";

export const useValidateBitcoin = (address: string) => {
  let addressString = address;
  let amount: string | undefined = "";
  let isValid = false;
  let isTapRoot = false;

  if (address.includes("bitcoin:"))
    addressString = addressString.replace("bitcoin:", "");
  if (address.includes("?amount=")) {
    amount = addressString.split("?amount=").pop();
    addressString = addressString.split("?amount=")[0];
  }

  isValid = validate(addressString);

  if (addressString.startsWith("bc1p")) {
    isValid = false;
    isTapRoot = true;
  }

  return {
    bitcoinAddress: addressString,
    amount,
    isValid,
    isTapRoot,
  };
};
