import { useMemo } from "react";
import { IUserToken } from "../models/Tokens";
import { formatRoundedDownTokenPrecision } from "../utils";
import {
  EXPECTED_FEE_DIVIDER,
  MAX_ASSET_FEE_MULTIPLIER,
} from "../utils/constants";
import useTokenInformation from "./queries/tokens/useTokenInformation";

interface IUseTokenPriceConversion {
  fromToken: IUserToken;
  toToken: IUserToken;
  amount: string;
  pool: string;
}

interface IUseTokenPriceConversionResponse {
  pricePerToken: string;
  slippage: string;
  minExpectedAmount: string;
}

const emptyResponse = {
  pricePerToken: "0.00",
  slippage: "0",
  minExpectedAmount: "0",
};

enum TokenName {
  PBTC = "PBTC",
  BTCL = "BTCL",
  USDL = "USDL",
  PUSDT = "PUSDT",
  LIBRE = "LIBRE",
}
type IQuantitiesMap = {
  [key in TokenName]?: string;
};

// WHOLE COMPONENT NEEDS TO BE RE-WRITTEN BEFORE PROD PUSH TO GET OFF TESTNET
export const useTokenPriceConversion = ({
  fromToken,
  toToken,
  amount,
  pool,
}: IUseTokenPriceConversion): IUseTokenPriceConversionResponse => {
  const { data: tokenData } = useTokenInformation();

  const priceInformation = useMemo(() => {
    if (!tokenData || !fromToken || !toToken) return emptyResponse;
    if (!amount) return emptyResponse;

    let quantitiesMap: IQuantitiesMap = {};

    if (pool === "BTCLIB") {
      quantitiesMap = {
        BTCL: tokenData[1].pool1.quantity.split(" ")[0],
        PBTC: tokenData[1].pool1.quantity.split(" ")[0],
        LIBRE: tokenData[1].pool2.quantity.split(" ")[0],
      };
    } else {
      quantitiesMap = {
        BTCL: tokenData[0].pool1.quantity.split(" ")[0],
        PBTC: tokenData[0].pool1.quantity.split(" ")[0],
        USDL: tokenData[0].pool2.quantity.split(" ")[0],
        PUSDT: tokenData[0].pool2.quantity.split(" ")[0],
      };
    }

    let fromTokenQuantity = Number(
      quantitiesMap[fromToken.symbol as keyof IQuantitiesMap]
    );
    let toTokenQuantity = Number(
      quantitiesMap[toToken.symbol as keyof IQuantitiesMap]
    );

    const pricePerToken = toTokenQuantity / fromTokenQuantity;

    const value = pricePerToken * Number(amount);
    const slippage = (Number(amount) / fromTokenQuantity) * 100;

    const fromTokenPoolAfterTrade = Number(fromTokenQuantity) + Number(amount);
    const expectedToAmount =
      (toTokenQuantity * Number(amount)) / fromTokenPoolAfterTrade;

    const expectedFee =
      expectedToAmount *
      (pool === "BTCLIB"
        ? tokenData[1].fee / EXPECTED_FEE_DIVIDER
        : tokenData[0].fee / EXPECTED_FEE_DIVIDER);
    // MIN FEE TKAEN OUT TO AVOID ISSUES WITH CONTRACT REQUIREMENTS
    // const minFee = MIN_FEE_MULTIPLIER ** (fromToken.precision - 1);

    const minExpectedAmount =
      expectedToAmount - expectedFee * MAX_ASSET_FEE_MULTIPLIER;

    if (Number(value) === 0.0) return emptyResponse;

    return {
      pricePerToken: formatRoundedDownTokenPrecision({
        value: pricePerToken,
        precision: toToken.precision,
      }),
      slippage,
      minExpectedAmount: formatRoundedDownTokenPrecision({
        value: minExpectedAmount,
        precision: toToken.precision,
      }),
    };
  }, [fromToken, toToken, tokenData, amount, pool]);

  return {
    pricePerToken: String(priceInformation.pricePerToken),
    slippage: Number(priceInformation.slippage).toFixed(2),
    minExpectedAmount:
      Number(priceInformation.minExpectedAmount) > 0
        ? String(priceInformation.minExpectedAmount)
        : "0",
  };
};
