import { useQuery, UseQueryResult } from "react-query";
import { IValidatorVote } from "../../../models/Validators";
import APIClient from "../../../services/APIClient";
import { RQ_VOTES } from "./keys";

const getValidatorVotes = async (accountName: string) => {
  if (!accountName) return undefined;
  const data: IValidatorVote = await APIClient.fetchValidatorVotes(accountName);
  return data;
};

export default function useValidatorVotes(
  accountName: string
): UseQueryResult<IValidatorVote | undefined, Error> {
  return useQuery<IValidatorVote | undefined, Error>(
    [RQ_VOTES, accountName],
    () => getValidatorVotes(accountName)
  );
}
