import { useQuery, UseQueryResult } from "react-query";
import APIClient from "../../../services/APIClient";
import { RQ_WRAPPING_ADRESSES } from "./keys";

export const getBTCWrappingAdress = async (accountName: string) => {
  if (!accountName) return { bitcoin: "" };
  const data: any = await APIClient.fetchBitcoinWrappingAdresses(accountName);
  return data.bitcoin;
};

export default function useGetBitcoinWrappingAddress(
  accountName: string
): UseQueryResult<string, Error> {
  return useQuery<string, Error>([RQ_WRAPPING_ADRESSES, accountName], () =>
    getBTCWrappingAdress(accountName)
  );
}
