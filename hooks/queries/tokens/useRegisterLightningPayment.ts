import { useMutation } from "react-query";
import ChainClient from "../../../services/ChainClient";

export const useRegisterLightningPayment = () => {
  return useMutation((payload: any) =>
    ChainClient.registerLightningPayment(payload)
  );
};
