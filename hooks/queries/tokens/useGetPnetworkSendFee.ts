import { useQuery } from "react-query";
import { IChain, IFeeData } from "../../../models/Fees";
import ChainClient from "../../../services/ChainClient";
import { RQ_PNETWORK_FEES } from "./keys";

export const BITCOIN_MAINNET_CHAIN_ID = "0x01ec97de";
export const ETHEREUM_MAINNET = "0x005fe7f9";
export const LIBRE_MAINNET = "0x026776fa";
export const PNETWORK_PRECISION = 1000000000000000000;

const fetchFees = async (tokenSymbol: string, amount: number) => {
  const response = await ChainClient.calculateFees(tokenSymbol);

  let chainId = "";
  const feeData: IFeeData = {
    networkFee: "",
    minimumProtocolFee: "",
    protocolFeeAmount: undefined,
    pegOutFee: undefined,
    pegInFee: undefined,
  };

  if (tokenSymbol === "PBTC") chainId = BITCOIN_MAINNET_CHAIN_ID;
  if (tokenSymbol === "PUSDT") chainId = ETHEREUM_MAINNET;
  if (tokenSymbol === "LIBRE") chainId = LIBRE_MAINNET;

  if (Array.isArray(response.result)) {
    // Find the specified chain in the JSON response
    const chain = response.result.find(
      (chain: IChain) => chain.chainId === chainId
    );

    if (chain) {
      // Calculate networkFee
      feeData.networkFee = (chain.fees.networkFee / PNETWORK_PRECISION).toFixed(
        6
      );

      // Calculate minimumProtocolFee
      feeData.minimumProtocolFee = (
        chain.fees.minNodeOperatorFee / PNETWORK_PRECISION
      ).toFixed(6);

      // Calculate protocolFeePercent in percentage
      let protocolFeePercent = undefined;
      if (chain.fees.basisPoints) {
        if (chain.fees.basisPoints.hostToNative !== undefined) {
          protocolFeePercent = chain.fees.basisPoints.hostToNative * 0.0001;
        } else if (chain.fees.basisPoints.nativeToNative !== undefined) {
          protocolFeePercent = chain.fees.basisPoints.nativeToNative * 0.0001;
        }
      }

      // Calculate protocolFeeAmount
      feeData.protocolFeeAmount =
        protocolFeePercent !== undefined
          ? Math.max(
              protocolFeePercent * amount,
              parseFloat(feeData.minimumProtocolFee)
            ).toFixed(6)
          : undefined;

      // Calculate pegOutFee
      feeData.pegOutFee =
        feeData.protocolFeeAmount !== undefined
          ? (
              parseFloat(feeData.protocolFeeAmount) +
              parseFloat(feeData.networkFee)
            ).toFixed(6)
          : undefined;

      const libreChain = response.result.find(
        (chain: IChain) => chain.chainId === LIBRE_MAINNET
      );

      if (libreChain) {
        // Calculate pegInFee
        const libreFeePercent =
          libreChain.fees.basisPoints.nativetoHost * 0.0001;
        feeData.pegInFee = Math.max(
          libreFeePercent * amount,
          parseFloat(feeData.minimumProtocolFee)
        ).toFixed(6);
      }
    }
  }
  return Number(feeData.pegOutFee);
};

export const useGetPnetworkSendFee = (tokenSymbol: string, amount: number) => {
  return useQuery<number, Error>({
    queryKey: [RQ_PNETWORK_FEES, tokenSymbol, amount],
    queryFn: () => fetchFees(tokenSymbol, amount),
    enabled: amount > 0 && !!tokenSymbol,
  });
};
