import { useContext } from "react";
import { useMutation, useQueryClient } from "react-query";
import { NotificationContext } from "../../../providers/NotificationProvider";
import LibreClient from "../../../services/LibreClient";
import { RQ_USER_POOL_SUPPLY } from "../tokens/keys";
import { RQ_FARMING_STATS, RQ_USER_FARMING } from "./keys";

export const useFarmingClaim = () => {
  const queryClient = useQueryClient();
  const { handleOnOpen: handleNotification } = useContext(NotificationContext);
  return useMutation((pool: string) => LibreClient.farmingClaim(pool), {
    onSuccess: async (data) => {
      if (data && data.success && data.transactionId)
        handleNotification(data.transactionId);
      queryClient.invalidateQueries([RQ_USER_FARMING]);
      queryClient.invalidateQueries([RQ_FARMING_STATS]);
      queryClient.invalidateQueries([RQ_USER_POOL_SUPPLY]);
    },
  });
};
