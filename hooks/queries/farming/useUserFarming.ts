import { useQuery, UseQueryResult } from "react-query";
import { IUserFarming } from "../../../models/Farming";
import APIClient from "../../../services/APIClient";
import { RQ_USER_FARMING } from "./keys";

const getUserFarming = async (accountName: string) => {
  if (!accountName) return [];
  const data: IUserFarming[] = await APIClient.fetchUserFarming(accountName);
  return data;
};

export default function useUserFarming(
  accountName: string
): UseQueryResult<IUserFarming[], Error> {
  return useQuery<IUserFarming[], Error>([RQ_USER_FARMING, accountName], () =>
    getUserFarming(accountName)
  );
}
