import { useQuery, UseQueryResult } from "react-query";
import { IFarmingStats } from "../../../models/Farming";
import APIClient from "../../../services/APIClient";
import { RQ_FARMING_STATS } from "./keys";

const getFarmingStats = async () => {
  const data: IFarmingStats[] = await APIClient.fetchFarmingStats();
  return data;
};

export default function useFarmingStats(): UseQueryResult<
  IFarmingStats[],
  Error
> {
  return useQuery<IFarmingStats[], Error>([RQ_FARMING_STATS], () =>
    getFarmingStats()
  );
}
