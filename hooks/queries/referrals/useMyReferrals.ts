import { useQuery, UseQueryResult } from "react-query";
import { IMyReferral } from "../../../models/Referrals";
import APIClient from "../../../services/APIClient";
import { RQ_MY_REFERRALS } from "./keys";

const getMyReferrals = async (name: string) => {
  const data: IMyReferral[] = await APIClient.fetchMyReferrals(name);
  return data;
};

export default function useMyReferrals(
  name: string
): UseQueryResult<IMyReferral[], Error> {
  return useQuery<IMyReferral[], Error>([RQ_MY_REFERRALS, name], () =>
    getMyReferrals(name)
  );
}
