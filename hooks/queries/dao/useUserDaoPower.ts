import { useQuery, UseQueryResult } from "react-query";
import { IUserDaoPower } from "../../../models/Dao";
import LibreClient from "../../../services/LibreClient";
import { RQ_USER_DAO_POWER } from "./keys";

const getUserDaoPower = async (accountName: string) => {
  if (!accountName) return null;
  const data = await LibreClient.getUserDaoPower(accountName);
  return data[0];
};

export default function useUserDaoPower(
  accountName: string
): UseQueryResult<IUserDaoPower, Error> {
  return useQuery<IUserDaoPower, Error>([RQ_USER_DAO_POWER, accountName], () =>
    getUserDaoPower(accountName)
  );
}
