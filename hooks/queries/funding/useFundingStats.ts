import { useQuery, UseQueryResult } from "react-query";
import APIClient from "../../../services/APIClient";
import { RQ_FUNDING_STATS } from "./keys";
import { IFundingStats } from "../../../models/Funding";

const getFundingStats = async () => {
  const data: IFundingStats = await APIClient.fetchFundingStats();
  return data;
};

export default function useFundingStats(): UseQueryResult<IFundingStats, Error> {
  return useQuery<IFundingStats, Error>([RQ_FUNDING_STATS], getFundingStats);
}
