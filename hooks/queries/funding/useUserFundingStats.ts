import { useQuery, UseQueryResult } from "react-query";
import APIClient from "../../../services/APIClient";
import { RQ_USER_FUNDING_STATS } from "./keys";
import { IUserFundingStats } from "../../../models/Funding";

const getUserFundingStats = async (accountName: string) => {
  const data: IUserFundingStats = await APIClient.fetchUserFundingStats(
    accountName
  );
  return data;
};

export default function useUserFundingStats(
  accountName: string
): UseQueryResult<IUserFundingStats, Error> {
  return useQuery<IUserFundingStats, Error>(
    [RQ_USER_FUNDING_STATS],
    () => getUserFundingStats(accountName),
    { enabled: !!accountName }
  );
}
