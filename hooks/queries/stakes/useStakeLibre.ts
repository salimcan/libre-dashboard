import { useMutation, useQueryClient } from "react-query";
import LibreClient from "../../../services/LibreClient";
import { RQ_USER_STAKES, RQ_USER_STAKE_STATS } from "./keys";

export const useStakeLibre = () => {
  const queryClient = useQueryClient();
  return useMutation(
    ({ quantity, duration }: { quantity: string; duration: string }) =>
      LibreClient.stakeLibre({ quantity, duration }),
    {
      onSuccess: async () => {
        queryClient.invalidateQueries(RQ_USER_STAKES);
        queryClient.invalidateQueries(RQ_USER_STAKE_STATS);
      },
    }
  );
};
