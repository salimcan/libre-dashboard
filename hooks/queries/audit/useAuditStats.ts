import { useQuery, UseQueryResult } from "react-query";
import { IAuditStats } from "../../../models/Audits";
import APIClient from "../../../services/APIClient";
import { RQ_AUDIT_STATS } from "./keys";

const getAuditStats = async () => {
  const data: IAuditStats = await APIClient.fetchAuditStats();
  return data;
};

export default function useAuditStats(): UseQueryResult<IAuditStats, Error> {
  return useQuery<IAuditStats, Error>([RQ_AUDIT_STATS], () => getAuditStats());
}
