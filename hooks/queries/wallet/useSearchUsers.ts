import { useQuery, useQueryClient, UseQueryResult } from "react-query";
import { ISearchUser } from "../../../models/Wallet";
import { RQ_SEARCH_USERS } from "./keys";

export default function useSearchUsers(): UseQueryResult<ISearchUser[], Error> {
  const queryClient = useQueryClient();
  return useQuery<any, Error>([RQ_SEARCH_USERS], () =>
    queryClient.getQueryData(RQ_SEARCH_USERS)
  );
}
