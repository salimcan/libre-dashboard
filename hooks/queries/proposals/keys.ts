export const RQ_PROPOSALS = "proposals";
export const RQ_PROPOSAL = "proposal";
export const RQ_VOTES = "votes";
export const RQ_PROPOSAL_PROPERTIES = "proposal_properties";
