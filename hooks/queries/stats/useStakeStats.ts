import { useQuery, UseQueryResult } from "react-query";
import { IStakeStats } from "../../../models/Stats";
import APIClient from "../../../services/APIClient";
import { RQ_STAKING_STATS } from "./keys";

const getStakeStats = async () => {
  const data: IStakeStats = await APIClient.fetchStakeStats();
  return data;
};

export default function useStakeStats(): UseQueryResult<IStakeStats, Error> {
  return useQuery<IStakeStats, Error>([RQ_STAKING_STATS], getStakeStats);
}
