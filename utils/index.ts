import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc"; // dependent on utc plugin
import { DefaultTheme } from "styled-components";
import { IProposal, IStatus } from "../models/Proposals";
import { SHORTENED_TOKEN_PRECISION } from "./constants";

dayjs.extend(utc);
dayjs.extend(timezone);

export interface QueryParams {
  collection_name?: string;
  template_id?: string;
  ids?: string;
  sort?: string;
  order?: string;
  symbol?: string;
  state?: number;
  limit?: number;
  page?: number;
}

export const toQueryString = (queryObject: QueryParams): string => {
  return Object.keys(queryObject)
    .map(
      (key) =>
        encodeURIComponent(key) + "=" + encodeURIComponent(queryObject[key])
    )
    .join("&");
};

export const capitalize = (word: string): string => {
  if (!word) return "";
  return word[0].toUpperCase() + word.slice(1);
};

export const formatNumber = (numberString: string): string =>
  numberString.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

export const asyncForEach = async (
  array: unknown[],
  callback: (element: unknown, index: number, array: unknown[]) => void
): Promise<void> => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};

export const parseTimestamp = (timestamp: string): string => {
  if (timestamp) {
    return `${dayjs(+timestamp)
      .tz("America/Los_Angeles")
      .format("MMM DD, YYYY, h:mm A")} PST`;
  }
  return "";
};

export const addPrecisionDecimal = (
  number: string,
  precision: number,
  noCommas?: boolean,
  removeZeros?: boolean
): string => {
  if (number && number.includes(".")) return formatThousands(number);
  if (number && number.length > precision) {
    const insertDecimalAtIndex = number.length - precision;
    const numberString =
      number.slice(0, insertDecimalAtIndex) +
      "." +
      number.slice(insertDecimalAtIndex);
    if (noCommas) {
      return numberString;
    }
    if (removeZeros) {
      return formatThousands(parseFloat(numberString).toString());
    }
    return formatThousands(numberString.toString());
  }

  let prependZeros = "";
  for (let i = 0; i < precision - number.length; i++) {
    prependZeros += "0";
  }
  const numberString = `0.${prependZeros + number}`;
  if (noCommas) {
    return numberString;
  }
  return formatThousands(parseFloat(numberString).toString());
};

export const formatPrice = (
  priceString: string,
  tokenPrecision?: number
): string => {
  if (!priceString) return "";
  const [price, currency] = priceString.split(" ");
  const amount = formatThousands(
    parseFloat(price.replace(/[,]/g, "")).toFixed(
      tokenPrecision || SHORTENED_TOKEN_PRECISION
    )
  );
  return `${amount} ${currency}`;
};

export const formatThousands = (numberString: string): string => {
  if (!numberString) return "";
  const [integers, decimals] = numberString.split(".");
  let salePrice = parseFloat(integers.replace(/[,]/g, "")).toLocaleString();
  salePrice = decimals ? salePrice + "." + decimals : salePrice;
  return salePrice;
};

export const shortenThousands = (number: Number): string | Number => {
  if (number >= 1000000000) {
    return (Number(number) / 1000000000).toFixed(1).replace(/\.0$/, "") + "G";
  }
  if (number >= 1000000) {
    return (Number(number) / 1000000).toFixed(1).replace(/\.0$/, "") + "M";
  }
  if (number >= 1000) {
    return (Number(number) / 1000).toFixed(1).replace(/\.0$/, "") + "K";
  }
  return number;
};

export const delay = (ms: number): Promise<void> =>
  new Promise((resolve) => setTimeout(resolve, ms));

export const satsToBitcoin = (satsValue: number) => satsValue / 100000000;
export const bitcoinToSats = (bitcoinAmount: number) =>
  Number((bitcoinAmount * 100000000).toFixed(0));

export const generateProposalId = () => {
  const acceptableChars = "abcdefghijklmnopqrstuvwxyz12345";
  let result = "";
  for (var i = 12; i > 0; --i)
    result +=
      acceptableChars[Math.round(Math.random() * (acceptableChars.length - 1))];
  return result;
};

export const getProposalStatus = (status: number) => {
  const statuses: IStatus = {
    1: "Draft",
    2: "Active",
    3: "Passed",
    4: "Failed",
    5: "Canceled",
    6: "Completed",
  };

  return statuses[status];
};

export const getStatusLabelColor = (status: number, theme: DefaultTheme) => {
  const statuses = {
    1: {
      backgroundColor: theme.labelGreyBackground,
      textColor: theme.labelGrey,
    },
    2: {
      backgroundColor: theme.labelOrangeBackground,
      textColor: theme.labelOrange,
    },
    3: {
      backgroundColor: theme.labelGreenBackground,
      textColor: theme.labelGreen,
    },
    4: {
      backgroundColor: theme.labelRedBackground,
      textColor: theme.labelRed,
    },
    5: {
      backgroundColor: theme.labelRedBackground,
      textColor: theme.labelRed,
    },
    6: {
      backgroundColor: theme.labelGreenBackground,
      textColor: theme.labelGreen,
    },
  };

  return statuses[status];
};

export const formatMoneyString = (labelValue: number) => {
  // Nine Zeroes for Billions
  return Math.abs(Number(labelValue)) >= 1.0e9
    ? Number(Math.abs(Number(labelValue)) / 1.0e9).toFixed(3) + " " + "B"
    : // Six Zeroes for Millions
    Math.abs(Number(labelValue)) >= 1.0e6
    ? Number(Math.abs(Number(labelValue)) / 1.0e6).toFixed(3) + " " + "MM"
    : // Three Zeroes for Thousands
    Math.abs(Number(labelValue)) >= 1.0e3
    ? Number(Math.abs(Number(labelValue)) / 1.0e3).toFixed(3) + " " + "K"
    : Math.abs(Number(labelValue));
};

export const renderVotesPercentage = ({
  isFor,
  proposal,
}: {
  isFor: boolean;
  proposal: IProposal;
}) => {
  if (!proposal) return "";
  const { votes_for, votes_against } = proposal;
  const total = Number(votes_for) + Number(votes_against);
  if (total === 0) return "0%";
  return `${String(
    ((isFor ? Number(votes_for) : Number(votes_against)) * 100) / total
  )}%`;
};

export const formatCurrencyWithPrecision = (
  amount: number,
  precision: number
) => {
  const intPartOfNumber = Math.trunc(amount)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

  const floatPartOfNumber = Number(amount.toFixed(precision))
    .toString()
    .split(".")[1];
  if (precision === 0 || !floatPartOfNumber) {
    return intPartOfNumber;
  }
  return `${intPartOfNumber}.${floatPartOfNumber}`;
};

export const findTokenSymbol = (symbol: string): string => {
  if (symbol === "PBTC" || symbol === "BTCL")
    return "/logos/bitcoin-circle.svg";
  if (symbol === "PUSDT" || symbol === "USDL")
    return "/icons/usdt-asset-icon.png";
  if (symbol === "LIBRE") return "/icons/libre-asset-icon.svg";
  return "";
};

export const countDecimals = (value: number) => {
  if (Math.floor(value) === value) return 0;
  var str = value.toString();
  if (str.indexOf(".") !== -1 && str.indexOf("-") !== -1) {
    return str.split("-")[1] || 0;
  } else if (str.indexOf(".") !== -1) {
    return str.split(".")[1].length || 0;
  }
  return str.split("-")[1] || 0;
};

export const formatRoundedDownTokenPrecision = ({
  value,
  precision,
}: {
  value: number;
  precision: number;
}): string => {
  const decimalPlaces = countDecimals(value);
  const precisionDivider = Number(String(1).padEnd(precision + 1, "0"));
  if (decimalPlaces < precision) return value.toFixed(precision);
  return (
    Math.floor(Number(value) * precisionDivider) / precisionDivider
  ).toFixed(precision);
};

export const usdToSats = (usdValue: number, bitcoinValue: number) =>
  Math.round((usdValue / bitcoinValue) * 100000000);
