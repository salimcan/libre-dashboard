import styled, { css } from "styled-components";
import useTokenInformation from "../../hooks/queries/tokens/useTokenInformation";
import { LoadingAnimation } from "../../uikit/LoadingAnimation";

const DetailsBox = styled.div`
  width: 100%;
  margin: 24px 0 0;
  padding: 20px 24px;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  max-width: 552px;
  width: 100%;
`;

const DetailsRow = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding: 8px 0;
`;

const DetailsText = styled.span<{
  red?: boolean;
  green?: boolean;
  isFetching?: boolean;
}>`
  display: flex;
  align-items: center;
  font-size: 14px;
  font-weight: 500;
  color: ${(p) => p.theme.detailBlockTitle};
  ${(p) =>
    p.red &&
    css`
      color: ${(p) => p.theme.detailBlockAccentRed};
    `};
  ${(p) =>
    p.green &&
    css`
      color: ${(p) => p.theme.detailBlockAccentGreen};
    `};
  ${(p) =>
    p.isFetching &&
    css`
      opacity: 0.7;
    `}
`;

const Loader = styled.div`
  margin-right: 5px;
`;

interface ISwapTokenDetailsBox {
  slippage: string;
  isFetchingPrice: boolean;
}

export const SwapTokenDetailsBox = ({
  slippage,
  isFetchingPrice,
}: ISwapTokenDetailsBox) => {
  const { data: tokenData } = useTokenInformation();

  if (!tokenData) return <></>;

  return (
    <DetailsBox>
      <DetailsRow>
        <DetailsText>Price Impact</DetailsText>
        <DetailsText green isFetching={isFetchingPrice}>
          {isFetchingPrice && (
            <Loader>
              <LoadingAnimation size="tiny" colorRGB="50, 205, 172" />
            </Loader>
          )}
          {`${slippage}%`}
        </DetailsText>
      </DetailsRow>
      <DetailsRow>
        <DetailsText>Liquidity Provider Fee</DetailsText>
        <DetailsText>{`${tokenData[0].fee / 100}%`}</DetailsText>
      </DetailsRow>
    </DetailsBox>
  );
};
