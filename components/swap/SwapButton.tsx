import { useContext } from "react";
import { useTheme } from "styled-components";
import { IUserToken } from "../../models/Tokens";
import { useAuthContext } from "../../providers/AuthProvider";
import { ConnectWalletContext } from "../../providers/ConnectWalletProvider";
import { MediumButton } from "../../uikit/Button";

interface ISwapButton {
  disabledFromTokens: string[];
  disabledToTokens: string[];
  activeTokenFrom: IUserToken;
  activeTokenTo: IUserToken;
  fromTokenValue: string | null;
  handleSubmit: () => void;
}

export const SwapButton = ({
  disabledFromTokens,
  disabledToTokens,
  activeTokenFrom,
  activeTokenTo,
  fromTokenValue,
  handleSubmit,
}: ISwapButton) => {
  const theme = useTheme();
  const { currentUser, isLoadingUser } = useAuthContext();
  const { handleOnOpen: handleLogin } = useContext(ConnectWalletContext);

  if (!currentUser || !currentUser.actor)
    return (
      <MediumButton
        disabled={isLoadingUser}
        text="Connect Wallet"
        color={theme.buttonOrangeText}
        backgroundColor={theme.buttonOrange}
        onClick={handleLogin}
      />
    );
  if (
    disabledFromTokens.includes(activeTokenFrom.symbol) ||
    disabledToTokens.includes(activeTokenTo.symbol)
  ) {
    return (
      <MediumButton
        disabled
        text="This pair is currently unavailable"
        color={theme.buttonGreyText}
        backgroundColor={"#f5f5f6"}
        border={theme.inputBorder}
        onClick={() => {}}
      />
    );
  }
  if (!fromTokenValue || Number(fromTokenValue) == 0) {
    return (
      <MediumButton
        disabled
        text="Enter an amount"
        color={theme.buttonGreyText}
        backgroundColor={"#f5f5f6"}
        border={theme.inputBorder}
        onClick={() => {}}
      />
    );
  }
  if (Number(fromTokenValue) > Number(activeTokenFrom.unstaked))
    return (
      <MediumButton
        disabled
        text={`Insufficient ${activeTokenFrom.name} balance`}
        color={theme.buttonGreyText}
        backgroundColor={"#f5f5f6"}
        border={theme.inputBorder}
        onClick={() => {}}
      />
    );
  return (
    <MediumButton
      text="Swap now"
      color={theme.buttonOrangeText}
      backgroundColor={theme.buttonOrange}
      onClick={handleSubmit}
    />
  );
};
