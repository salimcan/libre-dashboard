import { useLPToUSD } from "../../hooks/useLPToUSD";

export const USDPriceText = ({
  amount,
  pool,
}: {
  amount: number;
  pool: string;
}) => {
  const convertedPrice = useLPToUSD(amount, pool);
  return <>{convertedPrice}</>;
};
