import moment from "moment";
import { useContext, useMemo, useState } from "react";
import styled, { useTheme } from "styled-components";
import { useFundLibre } from "../../hooks/queries/funding/useFundLibre";
import useUserTokens from "../../hooks/queries/tokens/useUserTokens";
import { useAuthContext } from "../../providers/AuthProvider";
import { ConnectWalletContext } from "../../providers/ConnectWalletProvider";
import { MediumButton } from "../../uikit/Button";
import { InputSlider } from "../../uikit/InputSlider";
import { Notification } from "../../uikit/Notification";
import {
  bitcoinToSats,
  formatRoundedDownTokenPrecision,
  satsToBitcoin,
} from "../../utils";
import AppModal from "../AppModal";
import { calculateAPYAndPayout } from "../stake/StakeModal";

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const SliderContainer = styled.div`
  &:first-of-type {
    margin: 35px 0;
  }

  &:last-of-type {
    margin-bottom: 20px;
  }
`;

const BottomContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: -24px;
  margin-top: 24px;
  padding: 24px;
  background-color: #fafafa;
  border-top: 1px solid ${(p) => p.theme.detailBlockBorder};
  border-bottom-left-radius: 16px;
  border-bottom-right-radius: 16px;
`;

const BottomRow = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;

  &:last-of-type {
    margin-bottom: 40px;
  }
`;

const LeftText = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => p.theme.greyText};
`;

const RightText = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => p.theme.titleText};
`;

const Description = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => p.theme.greyText};
  margin-top: 20px;
`;

const Receive = styled.span`
  cursor: pointer;
  text-decoration: underline;
`;

interface IMintModal {
  open: boolean;
  handleOnClose: () => void;
}

export const FundModal = ({ open, handleOnClose }: IMintModal) => {
  const theme = useTheme();
  const { currentUser } = useAuthContext();
  const { handleOnOpen: handleLogin } = useContext(ConnectWalletContext);
  const { data: userTokens } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data"],
  });
  const { mutateAsync: fundLibre } = useFundLibre();

  const [amountValue, setAmountValue] = useState<string>("0");
  const [valueError, setValueError] = useState<boolean>(false);

  const handleAmountChange = (value: number) => {
    if (valueError) setValueError(false);
    setAmountValue(String(value));
  };

  const handleSubmit = async () => {
    if (Number(amountValue) < 1) return setValueError(true);
    const btcToken = userTokens?.find(
      (token) => token.symbol === "BTCL" || token.symbol === "PBTC"
    );
    if (!btcToken) return;
    const result = await fundLibre({
      quantity: formatRoundedDownTokenPrecision({
        value: satsToBitcoin(Number(amountValue)),
        precision: btcToken.precision,
      }),
      symbol: btcToken.symbol,
    });
    if (result && result.success) handleOnClose();
  };

  const contributionInitialValue = Number(amountValue);

  const btcTokenBalance = useMemo(() => {
    if (!userTokens || !userTokens.length) return 0;
    const token = userTokens.find(
      (t) => t.symbol === "PBTC" || t.symbol === "BTCL"
    );
    if (token) return token.total;
    return 0;
  }, [userTokens]);

  const renderButton = () => {
    if (!currentUser || !currentUser.actor) {
      return (
        <MediumButton
          onClick={handleLogin}
          text="Connect Wallet"
          color={theme.buttonOrangeText}
          backgroundColor={theme.buttonOrange}
        />
      );
    }
    return (
      <MediumButton
        onClick={handleSubmit}
        text="Contribute Now"
        color={theme.buttonOrangeText}
        backgroundColor={theme.buttonOrange}
      />
    );
  };

  if (!open) return <></>;

  return (
    <AppModal title="New Contribution" show={true} onClose={handleOnClose}>
      <Content>
        <Description>
          Your stake will have a start date of{" "}
          {moment().add(2, "days").format("MMM D, YYYY")}.
          <br />{" "}
        </Description>
        <SliderContainer>
          <InputSlider
            text="Your contribution (SATS):"
            valueSymbol="SATS"
            initialValue={contributionInitialValue}
            minText="0"
            min={0}
            maxText="MAX"
            max={Number(bitcoinToSats(btcTokenBalance))}
            handleUpdate={handleAmountChange}
            error={valueError}
            allowDecimals={false}
          />
        </SliderContainer>
        <BottomContainer>
          <BottomRow>
            <LeftText>Claim Date</LeftText>
            <RightText>TBD</RightText>
          </BottomRow>
          <BottomRow>
            <LeftText>Base APY</LeftText>
            <RightText>
              {Math.round(
                calculateAPYAndPayout(180, Number(amountValue)).apy * 100
              )}
              %
            </RightText>
          </BottomRow>
          <BottomRow>
            <LeftText>LIBRE Allocation</LeftText>
            <RightText>TBD</RightText>
          </BottomRow>
          <BottomRow>
            <LeftText>Claim Amount</LeftText>
            <RightText>TBD</RightText>
          </BottomRow>
          {valueError && (
            <>
              <Notification
                type="error"
                text={`Invalid Amount. Please input an amount above 1 SAT.`}
              />
              <br />
            </>
          )}
          {renderButton()}
        </BottomContainer>
      </Content>
    </AppModal>
  );
};
