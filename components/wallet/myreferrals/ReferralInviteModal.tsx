import styled, { useTheme } from "styled-components";
import { MediumButton } from "../../../uikit/Button";
import AppModal from "../../AppModal";

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.titleText};
`;

const Text = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  text-align: center;
  color: ${(p) => p.theme.greyText};
  display: block;
  max-width: 75%;
  margin-top: 16px;
`;

const BtnContainer = styled.div`
  margin-top: 40px;
  width: 100%;

  button {
    width: 100%;
  }
`;

interface IReferralInviteModal {
  open: boolean;
  handleOnClose: () => void;
}

export const ReferralInviteModal = ({
  open,
  handleOnClose,
}: IReferralInviteModal) => {
  const theme = useTheme();

  return (
    <AppModal title="" show={open} onClose={handleOnClose}>
      <Content>
        <Title>Download Bitcoin Libre</Title>
        <Text>
          Invite your friends to LIBRE and earn LIBRE for everyone who downloads
          the Bitcoin Libre Wallet, claim the Spin Drop and stake.
        </Text>
        <BtnContainer>
          <MediumButton
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
            text="Download Bitcoin Libre Wallet"
            onClick={() => {}}
          />
        </BtnContainer>
      </Content>
    </AppModal>
  );
};
