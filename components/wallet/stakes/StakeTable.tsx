import moment from "moment";
import { useState } from "react";
import {
  AutoSizer,
  Column,
  InfiniteLoader,
  Table,
  TableCellProps,
} from "react-virtualized";
import styled, { useTheme } from "styled-components";
import { useClaimLibre } from "../../../hooks/queries/stakes/useClaimStake";
import { IStake } from "../../../models/Stakes";
import { displayTokenPrecisions } from "../../../models/Tokens";
import { MediumButton } from "../../../uikit/Button";
import {
  headerRenderer,
  HEADER_HEIGHT,
  ROW_HEIGHT,
  TableWrapper,
  useGetTableInfo,
} from "../../../uikit/Table";
import { formatCurrencyWithPrecision } from "../../../utils";
import { MediaQueryWidths } from "../../../utils/constants";
import { EmergencyUnstakeModal } from "../../stake/EmergencyUnstakeModal";
import { TokenWithUSDValue } from "../../TokenWithUSDValue";

const CellItem = styled.span`
  font-size: 15px;
  color: black;
`;

const Date = styled.span`
  display: flex;
  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: none;
  }
`;

const DateMobile = styled.span`
  display: none;
  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: inline-block;
  }
`;

interface IStakeTable {
  data: IStake[];
  isLoading: boolean;
}

export const StakeTable = ({ data, isLoading }: IStakeTable) => {
  const theme = useTheme();
  const { resolveRowStyle, tableHeight, noRowsRenderer } = useGetTableInfo({
    data,
    isLoading,
    rows: 1,
  });
  const { mutate: claimStake } = useClaimLibre();

  const [activeStakeIndex, setActiveStakeIndex] = useState<number | null>(null);

  const isRowLoaded = ({ index }: { index: number }) => !!data[index];

  const loadMoreRows = async () => {
    return;
  };

  const cellRenderer = ({ cellData, dataKey, rowData }: TableCellProps) => {
    if (dataKey === "index") {
      const payoutDate = moment.utc(rowData.payout_date);
      const rightNowDate = moment.utc();
      if (rowData.status === 2) {
        return (
          <MediumButton
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
            text="Claimed"
            disabled
            onClick={() => {}}
            buttonStyle="emptyGradient"
          />
        );
      }
      if (payoutDate.isBefore(rightNowDate)) {
        return (
          <MediumButton
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
            text="Claim"
            onClick={() => claimStake(cellData)}
          />
        );
      }
      return (
        <MediumButton
          color={theme.buttonRedText}
          backgroundColor={theme.buttonRed}
          text="Emergency Unstake"
          onClick={() => setActiveStakeIndex(cellData)}
        />
      );
    }
    if (dataKey === "payout_date") {
      return (
        <CellItem>
          <Date>
            {moment.utc(cellData).local().format("MMM Do YYYY, h:mm:ss a")}
          </Date>
          <DateMobile>
            {moment.utc(cellData).local().format("MM/DD/YY, h:mm:ss a")}
          </DateMobile>
        </CellItem>
      );
    }
    if (dataKey === "apy") {
      return <CellItem>{Number(cellData).toFixed(2) + "%"}</CellItem>;
    }
    if (dataKey === "libre_staked" || dataKey === "payout") {
      if (
        cellData &&
        cellData.split(" ") &&
        cellData.split(" ")[0] &&
        cellData.split(" ")[1]
      ) {
        return (
          <CellItem>
            <TokenWithUSDValue
              tokenSymbol={"LIBRE"}
              value={formatCurrencyWithPrecision(
                Number(cellData.split(" ")[0]),
                displayTokenPrecisions[cellData.split(" ")[1]]
              )}
            />
          </CellItem>
        );
      }
      return <CellItem>--</CellItem>;
    }
    return <CellItem>{cellData}</CellItem>;
  };

  return (
    <>
      <TableWrapper height={tableHeight}>
        <AutoSizer>
          {({ width, height }) => (
            <InfiniteLoader
              isRowLoaded={isRowLoaded}
              loadMoreRows={loadMoreRows}
              rowCount={1}
            >
              {({ onRowsRendered, registerChild }) => (
                <Table
                  ref={registerChild}
                  width={width < 900 ? 900 : width}
                  height={height}
                  headerHeight={HEADER_HEIGHT}
                  rowHeight={ROW_HEIGHT}
                  rowCount={data.length}
                  rowGetter={({ index }) => data[index]}
                  rowStyle={({ index }) =>
                    resolveRowStyle(index + 1 === data.length)
                  }
                  onRowsRendered={onRowsRendered}
                  noRowsRenderer={noRowsRenderer}
                  overscanRowCount={5}
                >
                  <Column
                    label="Claim Date"
                    dataKey={"payout_date"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                  <Column
                    label="APY"
                    dataKey={"apy"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                  <Column
                    label="Initial Stake"
                    dataKey={"libre_staked"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                  <Column
                    label="Payout"
                    dataKey={"payout"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                  <Column
                    label="Action"
                    dataKey={"index"}
                    flexGrow={1}
                    flexShrink={1}
                    width={100}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                </Table>
              )}
            </InfiniteLoader>
          )}
        </AutoSizer>
      </TableWrapper>
      <EmergencyUnstakeModal
        open={!!activeStakeIndex}
        handleOnClose={() => setActiveStakeIndex(null)}
        stakeIndex={activeStakeIndex}
      />
    </>
  );
};

export default StakeTable;
