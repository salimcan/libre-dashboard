import { useContext, useState } from "react";
import styled, { css, useTheme } from "styled-components";
import { WalletSendContext } from "../../../providers/WalletSendProvider";
import { MediumButton } from "../../../uikit/Button";

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  margin-top: 10px;
  transition: 400ms all ease;
`;

const Content = styled.div<{ active?: boolean }>`
  display: flex;
  flex-direction: column;
  background: ${(p) => p.theme.detailBlockBackground};
  max-width: 552px;
  min-height: 100px;
  max-height: 200px;
  width: 100%;
  transition: 500ms all ease;
  opacity: 0;
  pointer-events: none;
  display: none;

  ${(p) =>
    p.active &&
    css`
      min-height: 0;
      opacity: 1;
      pointer-events: all;
      display: flex;
    `}
`;

const TextArea = styled.textarea`
  border-radius: 8px;
  border: 1px solid #c0c0c7;
  padding: 12px 14px;
  outline: none;
  margin-bottom: 20px;
  font-size: 15px;
  font-weight: 400;
  -webkit-appearance: none;
`;

export const SendSubmit = ({ active }: { active?: boolean }) => {
  const [value, setValue] = useState<string>("");

  const theme = useTheme();
  const { recipient, amount, resetState, submitSend, error, setModalOpen } =
    useContext(WalletSendContext);

  const handleChange = ({ target }) => setValue(target.value);
  const handleSubmit = async () => {
    try {
      await submitSend();
      resetState();
      setModalOpen(false);
    } catch (e) {}
  };

  const renderBtn = () => {
    if (error && error.length) {
      return (
        <MediumButton
          onClick={handleSubmit}
          text={error}
          color={theme.buttonGreyText}
          backgroundColor={theme.buttonGrey}
          disabled={true}
        />
      );
    }
    if (!recipient || !amount) {
      return (
        <MediumButton
          onClick={() => {}}
          text={"Send Now"}
          color={theme.buttonGreyText}
          backgroundColor={theme.buttonGrey}
          disabled={true}
        />
      );
    }
    return (
      <MediumButton
        onClick={handleSubmit}
        text={"Send Now"}
        color={theme.buttonOrangeText}
        backgroundColor={theme.buttonOrange}
        disabled={false}
      />
    );
  };

  return (
    <>
      <Wrapper>
        <Content active={active}>
          <TextArea
            placeholder="Leave a note (optional)"
            rows={5}
            onChange={handleChange}
            value={value}
          />
          {renderBtn()}
        </Content>
      </Wrapper>
    </>
  );
};
