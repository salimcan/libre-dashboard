import { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import { useGetLightningFee } from "../../../hooks/queries/tokens/useGetLightningFee";
import { useGetPnetworkSendFee } from "../../../hooks/queries/tokens/useGetPnetworkSendFee";
import { useAuthContext } from "../../../providers/AuthProvider";
import { WalletSendContext } from "../../../providers/WalletSendProvider";
import { LoadingAnimation } from "../../../uikit/LoadingAnimation";
import { bitcoinToSats } from "../../../utils";
import { CHAIN_CONFIG } from "../../../utils/constants";

const Recipient = styled.span`
  display: flex;
  align-items: center;
  color: ${(p) => p.theme.buttonGreyText};
  word-break: break-word;
`;

export const SendFee = () => {
  const [lightningFee, setLightningFee] = useState<number>(0);

  const { selectedToken, recipient, amount, setFee } =
    useContext(WalletSendContext);
  const { currentUser } = useAuthContext();

  const { mutateAsync: fetchLightningFee, isLoading: isLoadingLightningFee } =
    useGetLightningFee();
  const {
    data: pNetworkFee,
    isLoading: isLoadingPnetworkFee,
    isFetching: isFetchingPnetworkFee,
  } = useGetPnetworkSendFee(selectedToken?.symbol, amount);

  const handleLightningFee = async () => {
    if (!recipient || !amount) return setLightningFee(0);
    if (recipient.type !== "lightning") return;
    const { fee: lightningFee } = await fetchLightningFee({
      invoice: recipient?.destination,
      accountName: currentUser.actor,
    });
    setLightningFee(lightningFee);
  };

  useEffect(() => {
    handleLightningFee();
  }, [selectedToken?.symbol, recipient?.type, amount]);

  useEffect(() => {
    if (recipient?.type === "lightning") return setFee(lightningFee);
    return setFee(pNetworkFee);
  }, [recipient, lightningFee, pNetworkFee]);

  const renderFee = () => {
    if (recipient?.type === "libre") return 0;
    if (
      isLoadingLightningFee ||
      (isFetchingPnetworkFee && isLoadingPnetworkFee)
    )
      return <LoadingAnimation size="tiny" />;
    if (!recipient) return "--";
    if (recipient?.type === "lightning") {
      if (amount === 0) return "--";
      return `${Number(lightningFee).toLocaleString("en-US")}`;
    }
    if (recipient.type === "ethereum") {
      return pNetworkFee;
    }
    if (pNetworkFee) {
      return `${bitcoinToSats(pNetworkFee).toLocaleString("en-US")}`;
    }
    return "--";
  };

  const renderLabel = () => {
    let unit =
      recipient?.type === "ethereum" ? CHAIN_CONFIG().usdt.symbol : "SATS";
    return unit;
  };

  return (
    <Recipient>
      {renderFee()}
      &nbsp;{renderLabel()}
    </Recipient>
  );
};
