import styled from "styled-components";

const List = styled.div`
  display: flex;
  flex-direction: column;
`;

const ListItem = styled.div`
  display: flex;

  &:not(:last-of-type) {
    margin-bottom: 10px;
  }
`;

const Item = styled.div`
  --background-color: #e7e9ef;
  --width: 100%;

  animation: shimmer 1.25s infinite;
  background-color: var(--background-color);
  background-repeat: no-repeat;
  background-image: linear-gradient(
    to right,
    var(--background-color) 0%,
    white 50%,
    var(--background-color) 100%
  );
  border-radius: 0.5rem;
  height: 34px;
  opacity: 0.5;
  width: var(--width);

  @keyframes shimmer {
    0% {
      background-position: calc(var(--width) * -1) 0;
    }

    50% {
      opacity: 0.25;
    }

    100% {
      background-position: var(--width) 0;
    }
  }
`;

const Img = styled.div`
  --background-color: #e7e9ef;
  --width: 34px;

  animation: shimmer 1.25s infinite;
  background-color: var(--background-color);
  background-repeat: no-repeat;
  background-image: linear-gradient(
    to right,
    var(--background-color) 0%,
    white 50%,
    var(--background-color) 100%
  );
  border-radius: 50%;
  height: 34px;
  opacity: 0.5;
  width: var(--width);
  margin-right: 10px;
  flex-shrink: 0;

  @keyframes shimmer {
    0% {
      background-position: calc(var(--width) * -1) 0;
    }

    50% {
      opacity: 0.25;
    }

    100% {
      background-position: var(--width) 0;
    }
  }
`;

export const UserListSkeleton = () => {
  return (
    <List>
      {Array.from(Array(1).keys()).map((r) => (
        <ListItem key={r}>
          <Img />
          <Item />
        </ListItem>
      ))}
    </List>
  );
};
