import Image from "next/image";
import { QRCode } from "react-qrcode-logo";
import styled from "styled-components";

import { useEffect, useState } from "react";
import BTCImage from "../../../public/icons/lightning-icon.svg";

function fmtMSS(s: number) {
  return (s - (s %= 60)) / 60 + (9 < s ? ":" : ":0") + s;
}

const Container = styled.div`
  background-color: #fafafa;
  margin: -24px;
  border-top: 1px solid ${(p) => p.theme.labelGreenBackground};
  border-bottom: 1px solid ${(p) => p.theme.labelGreenBackground};
`;

const BottomContainer = styled.div`
  margin-top: 40px;
  display: flex;
  justify-content: space-between;
`;

const BottomTableContainer = styled.div`
  border-radius: 8px;
  border: solid 1px #e6e6e6;
  margin-top: 30px;
`;

const TableRow = styled.div`
  display: flex;
  justify-content: space-between;
`;

const TableRowTitle = styled.div`
  margin: 22px 24px 12px;
`;

const TableRowValue = styled.div`
  margin: 22px 24px 12px;
  color: #666;
`;

const QRCodeContainer = styled.div`
  width: 190px;
  margin: auto;
  background-color: ${(p) => p.theme.buttonWhite};
  padding: 10px;
  border-radius: 16px;
  margin-top: 40px;
  margin-bottom: 40px;
  border: solid 1px #e6e6e6;
`;

const HeaderTitle = styled.div`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.titleText};
`;

const Description = styled.div`
  color: ${(p) => p.theme.labelGrey};
  font-size: 14px;
  margin-top: 10px;
  overflow-wrap: break-word;
`;

export const LightningQRCodeModal = ({
  invoice,
  amount,
  setInvoice,
}: {
  invoice: string;
  amount: string;
  setInvoice: React.Dispatch<React.SetStateAction<string>>;
}) => {
  const [secondsRemaining, setSecondsRemaining] = useState(3599);

  useEffect(() => {
    const interval = setInterval(() => {
      setSecondsRemaining((value) => {
        return value - 1;
      });
    }, 1000);
    if (secondsRemaining === 0) {
      setInvoice("");
    }
    return () => {
      clearInterval(interval);
    };
  }, []);

  const handleCopyToClipboard = () => {
    navigator.clipboard.writeText(invoice);
  };

  return (
    <>
      <Container>
        <QRCodeContainer>
          <QRCode
            value={invoice}
            qrStyle="dots"
            eyeRadius={5}
            logoImage={BTCImage.src}
            logoHeight={50}
            logoWidth={50}
          />
        </QRCodeContainer>
      </Container>
      <BottomContainer>
        <div style={{ width: "90%" }}>
          <HeaderTitle>Lightning Invoice</HeaderTitle>
          <Description>{invoice}</Description>
        </div>
        <Image
          src="/icons/copy-icon.svg"
          width={24}
          height={24}
          alt="copy icon"
          onClick={handleCopyToClipboard}
          style={{ cursor: "pointer", width: "10%" }}
        />
      </BottomContainer>
      <BottomTableContainer>
        <TableRow style={{ borderBottom: "solid 1px #e6e6e6" }}>
          <TableRowTitle>Expiration</TableRowTitle>
          <TableRowValue>{fmtMSS(secondsRemaining)}</TableRowValue>
        </TableRow>
        <TableRow>
          <TableRowTitle>Amount</TableRowTitle>
          <TableRowValue>
            {Number(amount).toLocaleString("en-US")} SATS
          </TableRowValue>
        </TableRow>
      </BottomTableContainer>
    </>
  );
};
