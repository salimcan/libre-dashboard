import { orderBy } from "lodash";
import moment from "moment";
import { useCallback, useState } from "react";
import Lottie from "react-lottie";
import styled, { useTheme } from "styled-components";
import { DaoSkeleton } from "../../components/dao/DaoSkeleton";
import { ProposalListItem } from "../../components/dao/ProposalListItem";
import useProposals from "../../hooks/queries/proposals/useProposals";
import DaoEmptyState from "../../uikit/animations/dao-empty-state.json";
import { MediumButton } from "../../uikit/Button";
import { NoRowsWrapper } from "../../uikit/Table";
import { PageTitle } from "../PageTitle";
import { ViewMore } from "./ViewMore";

const Block = styled.div`
  padding: 0 24px 0;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  background-color: #fff;
`;

const EmptyStateContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 100px 20px;
  text-align: center;
`;

const EmptyStateTitle = styled.div`
  font-size: 19.2px;
  font-weight: 600;
  margin-top: 15px;
  margin-bottom: 15px;
`;

const defaultOptions = {
  loop: false,
  autoplay: true,
  animationData: DaoEmptyState,
};

export const DaoProposals = ({
  handleModalOpen,
}: {
  handleModalOpen: () => void;
}) => {
  const theme = useTheme();
  const { data, isLoading } = useProposals();

  const [showAll, setShowAll] = useState<boolean>(false);

  const renderItems = useCallback(() => {
    if (isLoading) {
      return (
        <NoRowsWrapper>
          {Array.from(Array(5).keys()).map((r) => (
            <DaoSkeleton key={r} />
          ))}
        </NoRowsWrapper>
      );
    }
    if ((!data || !data.length) && !isLoading)
      return (
        <EmptyStateContainer>
          <Lottie options={defaultOptions} width={120} height={120} />
          <EmptyStateTitle>Create a new proposal</EmptyStateTitle>
          <MediumButton
            text="Create Proposal"
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
            onClick={handleModalOpen}
          />
        </EmptyStateContainer>
      );
    const sortedData = orderBy(
      data,
      (d) => {
        return moment(d.expires_on);
      },
      ["desc"]
    );

    if (!showAll)
      return sortedData
        .slice(0, 5)
        .map((item, i) => (
          <ProposalListItem
            key={item.name}
            proposal={item}
            lastItem={i === sortedData.length - 1 && sortedData.length <= 5}
          />
        ));
    return sortedData.map((item) => (
      <ProposalListItem key={item.name} proposal={item} />
    ));
  }, [data, showAll]);

  return (
    <>
      <PageTitle title="Recent Proposals" />
      <Block>
        {renderItems()}
        {data && data?.length > 5 && (
          <ViewMore
            arrowVertical
            flip={showAll}
            onClick={() => setShowAll(!showAll)}
          >
            {showAll ? `View Less Proposals` : `View More Proposals`}
          </ViewMore>
        )}
      </Block>
    </>
  );
};
