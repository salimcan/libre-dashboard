import Image from "next/image";
import styled, { useTheme } from "styled-components";
import useUserDaoPower from "../../hooks/queries/dao/useUserDaoPower";
import { useVoteProducer } from "../../hooks/queries/validators/useVoteProducer";
import { useAuthContext } from "../../providers/AuthProvider";
import { MediumButton } from "../../uikit/Button";
import AppModal from "../AppModal";

const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const Title = styled.span`
  text-align: center;
  font-size: 19.2px;
`;

const Description = styled.span`
  display: block;
  align-self: center;
  text-align: center;
  color: ${(p) => p.theme.greyText};
  font-weight: 500;
  font-size: 14px;
  max-width: 70%;
  margin: 20px 0 40px;
`;

const ImageContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 40px 0 30px;
`;

interface IStakeModal {
  open: boolean;
  handleOnClose: () => void;
  producer: string;
}

export const VoteModal = ({ open, handleOnClose, producer }: IStakeModal) => {
  const { currentUser } = useAuthContext();
  const { data: userPower, isLoading: isLoadingPower } = useUserDaoPower(
    currentUser.actor
  );

  const theme = useTheme();
  const { mutate: voteProducer } = useVoteProducer();

  const handleSubmit = async () => {
    voteProducer(producer);
    handleOnClose();
  };

  const renderEmpty = () => {
    return (
      <Content>
        <ImageContainer>
          <Image src={"/images/voting-power.png"} width={100} height={155} />
        </ImageContainer>
        <Title>You Need Voting Power</Title>
        <Description>
          In order to vote for validators you first need to stake your tokens.
        </Description>
      </Content>
    );
  };

  const renderContent = () => {
    if (!isLoadingPower && Number(userPower?.voting_power) === 0)
      return renderEmpty();
    return (
      <Content>
        <ImageContainer>
          <Image src={"/images/validators.png"} width={80} height={80} />
        </ImageContainer>
        <Title>Please Confirm Vote</Title>
        <Description>
          Your about to vote for this validator. You can change your vote at
          anytime.
        </Description>
        <MediumButton
          onClick={handleSubmit}
          text="Confirm Vote"
          color={theme.buttonOrangeText}
          backgroundColor={theme.buttonOrange}
        />
      </Content>
    );
  };

  if (!open) return <></>;

  return (
    <AppModal title="" show={true} onClose={handleOnClose}>
      {renderContent()}
    </AppModal>
  );
};
