import Link from "next/link";
import styled from "styled-components";
import { SmallButton } from "../../uikit/Button";
import { MediaQueryWidths } from "../../utils/constants";
import { ButtonContainer, Description, Title } from "./Announcements";

const Wrapper = styled.div`
  flex: 0 1 49%;
  border-radius: 16px;
  background-color: ${(p) => p.theme.leftAnnouncementBackground};
  background-image: url("/images/wheel.png");
  background-size: 100%;
  background-repeat: no-repeat;
  background-position: bottom 75% right -250px;
  padding: 25px;
  padding-bottom: 50px;
  position: relative;

  @media (max-width: ${MediaQueryWidths.small}px) {
    width: 100%;
    overflow: hidden;
  }
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 80%;
`;

export const LeftAnnouncement = () => {
  return (
    <Wrapper
      onClick={() => window.open("https://spindrop.libre.org/", "_blank")}
    >
      <Container>
        <Title>
          Libre SpinDrop is <span>currently live</span>!
        </Title>
        <Description color="#898ca9">
          If you have an ETH, SOL, EOS, TLOS, WAX or Proton wallet, you may be
          eligible for the spindrop.
        </Description>
        <ButtonContainer>
          <Link href="https://spindrop.libre.org/" passHref>
            <a target="_blank">
              <SmallButton text="Claim Now" onClick={() => {}} />
            </a>
          </Link>
        </ButtonContainer>
      </Container>
    </Wrapper>
  );
};
