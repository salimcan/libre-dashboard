import Image from "next/image";
import {
  AutoSizer,
  Column,
  Index,
  InfiniteLoader,
  Table,
  TableCellProps,
} from "react-virtualized";
import styled from "styled-components";
import useTokens from "../../../hooks/queries/tokens/useTokens";
import { displayTokenPrecisions } from "../../../models/Tokens";
import { SectionEmpty } from "../../../uikit/SectionEmpty";
import {
  headerRenderer,
  HEADER_HEIGHT,
  ROW_HEIGHT,
  TableWrapper,
  useGetTableInfo,
} from "../../../uikit/Table";
import { formatCurrencyWithPrecision } from "../../../utils";

const CellItem = styled.span`
  display: flex;
  align-items: center;
  font-size: 15px;
  color: black;
`;

const AssetIcon = styled.div`
  margin-right: 15px;
`;

export const TokenTable = () => {
  const { data = [], isLoading, isFetched } = useTokens();

  const isRowLoaded = ({ index }: Index) => !!data[index];
  const { resolveRowStyle, tableHeight, noRowsRenderer } = useGetTableInfo({
    data,
    isLoading,
    rows: 3,
  });

  const loadMoreRows = async () => {
    return;
  };

  const cellRenderer = ({ cellData, dataKey, rowData }: TableCellProps) => {
    if (dataKey === "name") {
      let icon = "/icons/libre-asset-icon.svg";
      if (String(cellData) === "LIBRE") icon = "/icons/libre-asset-icon.svg";
      if (String(cellData) === "PUSDT") icon = "/icons/usdt-asset-icon.png";
      if (String(cellData) === "PBTC") icon = "/icons/btc-asset-icon.svg";
      return (
        <CellItem>
          <AssetIcon>
            <Image src={icon} width={33} height={32} />
          </AssetIcon>
          {cellData}
        </CellItem>
      );
    }
    if (dataKey === "supply") {
      return (
        <CellItem>
          {formatCurrencyWithPrecision(
            Number(cellData),
            displayTokenPrecisions[rowData.name]
          )}
        </CellItem>
      );
    }

    if (dataKey === "marketCap") {
      return (
        <CellItem>${formatCurrencyWithPrecision(cellData || 0, 2)}</CellItem>
      );
    }

    if (dataKey === "staked") {
      return <CellItem>{Number(cellData).toFixed(2)}%</CellItem>;
    }

    return (
      <CellItem>
        {cellData ? Number(cellData).toLocaleString("en-US") : "--"}
      </CellItem>
    );
  };

  if (isFetched && (!data || !data.length)) return <SectionEmpty />;

  return (
    <TableWrapper height={tableHeight}>
      <AutoSizer>
        {({ width, height }) => (
          <InfiniteLoader
            isRowLoaded={isRowLoaded}
            loadMoreRows={loadMoreRows}
            rowCount={1}
          >
            {({ onRowsRendered, registerChild }) => (
              <Table
                autoHeight={true}
                ref={registerChild}
                width={width < 700 ? 700 : width}
                height={height}
                headerHeight={HEADER_HEIGHT}
                rowHeight={ROW_HEIGHT}
                rowCount={data.length}
                rowGetter={({ index }) => data[index]}
                rowStyle={({ index }) =>
                  resolveRowStyle(index + 1 === data.length)
                }
                onRowsRendered={onRowsRendered}
                overscanRowCount={5}
                noRowsRenderer={noRowsRenderer}
              >
                <Column
                  label="Assets"
                  dataKey="name"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Circulating Supply"
                  dataKey="supply"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Market Cap"
                  dataKey="marketCap"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Staked %"
                  dataKey="staked"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
              </Table>
            )}
          </InfiniteLoader>
        )}
      </AutoSizer>
    </TableWrapper>
  );
};
