import styled from "styled-components";
import { MediaQueryWidths } from "../utils/constants";

const Wrapper = styled.div`
  flex-direction: column;
  display: flex;
`;

const Title = styled.h1`
  margin: 32px 0 32px 0;
  font-size: 27.7px;
  font-weight: 600;
  line-height: 1.45;
  color: ${(p) => p.theme.titleText};

  @media (max-width: ${MediaQueryWidths.small}px) {
    margin: 15px 0 15px 0px;
  }
`;

const Description = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: #9695a0;
  margin-top: -26px;
  margin-bottom: 26px;

  a {
    color: ${(p) => p.theme.textAccentOrange};
    text-decoration: underline;
    cursor: pointer;
  }

  @media (max-width: ${MediaQueryWidths.small}px) {
    margin: 0 0 15px 10px;
  }
`;

interface IPageTitle {
  title: string;
  description?: any;
}

export const PageTitle = ({ title, description }: IPageTitle) => {
  return (
    <Wrapper>
      <Title>{title}</Title>
      {description && <Description>{description}</Description>}
    </Wrapper>
  );
};
