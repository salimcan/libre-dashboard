import dynamic from "next/dynamic";
import { useEffect, useState } from "react";
import styled from "styled-components";

const ReactSlider = dynamic(import("react-slider"), { ssr: false });

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const TextColumn = styled.div`
  display: flex;
  flex-direction: column;
`;

const Text = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => p.theme.greyText};
  margin-right: 5px;
`;

const Values = styled.span`
  font-size: 16px;
  font-weight: 500;
  line-height: 1.5;
  color: ${(p) => p.theme.titleText};
`;

const SliderRow = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const MinMaxRow = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const MinMaxText = styled.span`
  font-size: 10.2px;
  font-weight: 600;
  line-height: 1.56;
  color: ${(p) => p.theme.greyText};
  text-transform: uppercase;
`;

const SliderContainer = styled.div`
  display: flex;
  align-items: center;
  height: 50px;
`;

const StyledSlider = styled(ReactSlider)`
  width: 100%;
  height: 4px;
  display: flex;
  align-items: center;
`;

const StyledThumb = styled.div`
  height: 28px;
  width: 28px;
  cursor: grab;
  background: #ffffff;
  border-radius: 50%;
  outline: none;

  -webkit-box-shadow: 0px 2px 6px 1px rgba(0, 0, 0, 0.15);
  box-shadow: 0px 2px 6px 1px rgba(0, 0, 0, 0.15);
`;

const Thumb = (props, state) => <StyledThumb {...props}></StyledThumb>;

const StyledTrack = styled.div`
  top: 0;
  bottom: 0;
  background: ${(props) =>
    props.index !== 1
      ? props.theme.sliderBarBackground
      : props.theme.sliderBarInactive};
  border-radius: 999px;
`;

const Track = (props, state) => <StyledTrack {...props} index={state.index} />;

const Input = styled.input`
  border: 1px solid #c0c0c7;
  padding: 10px 16px 10px;
  border-radius: 8px;
  font-size: 14px;
  margin: 8px 0 5px;

  ::placeholder,
  ::-webkit-input-placeholder {
    color: ${(p) => p.theme.titleText};
    font-weight: 500;
  }
  :-ms-input-placeholder {
    color: ${(p) => p.theme.titleText};
    font-weight: 500;
  }
`;

interface IStakeModalSlider {
  text: string;
  valueSymbol: string;
  initialValue: number;
  minText: string;
  min: number;
  maxText: string;
  max: number;
  handleUpdate: (value: number) => void;
}

export const StakeModalSlider = ({
  text,
  valueSymbol,
  initialValue,
  minText,
  min,
  maxText,
  max,
  handleUpdate,
}: IStakeModalSlider) => {
  const [value, setValue] = useState<number>(0);

  const handleOnChange = (value: number) => setValue(Number(value));
  const handleInputChange = (e) => {
    let { value, min, max } = e.target;
    value = Math.max(Number(min), Math.min(Number(max), Number(value)));
    setValue(Number(value));
  };

  useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  return (
    <Wrapper>
      <TextColumn>
        <Text>{text}</Text>
        <Input value={value} onChange={handleInputChange} min={min} max={max} />
      </TextColumn>
      <SliderRow>
        <SliderContainer>
          <StyledSlider
            renderTrack={Track}
            renderThumb={Thumb}
            onChange={(value) => handleOnChange(Number(value))}
            onAfterChange={(value) => handleUpdate(Number(value))}
            min={min}
            max={max}
            value={value}
          />
        </SliderContainer>
        <MinMaxRow>
          <MinMaxText>{minText}</MinMaxText>
          <MinMaxText>{maxText}</MinMaxText>
        </MinMaxRow>
      </SliderRow>
    </Wrapper>
  );
};
