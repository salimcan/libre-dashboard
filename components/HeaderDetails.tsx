import Image from "next/image";
import { useMemo } from "react";
import styled from "styled-components";
import useTokens from "../hooks/queries/tokens/useTokens";
import { useExchangeRates } from "../hooks/useExchangeRates";
import { IconPairs } from "../uikit/IconPairs";
import { MediaQueryWidths } from "../utils/constants";

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  gap: 20px;
  justify-content: space-between;
  align-items: center;
  margin-top: -10px;
  margin-bottom: 30px;

  @media (max-width: ${MediaQueryWidths.small}px) {
    margin-bottom: 15px;
  }
`;

const Item = styled.div`
  background: #ffffff;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
  padding: 20px 20px;
  display: flex;
  flex: 1;
  align-items: center;
  border-radius: 16px;
  align-self: flex-end;

  @media (max-width: ${MediaQueryWidths.small}px) {
    padding: 10px;
  }
`;

const Label = styled.span`
  display: flex;
  align-items: center;
  font-size: 15px;
  text-transform: uppercase;
  margin-right: 5px;
  line-height: 1;

  @media (max-width: ${MediaQueryWidths.medium}px) {
    display: none !important;
  }

  span {
    @media (max-width: ${MediaQueryWidths.small}px) {
      display: none !important;
    }
  }
`;

const Col = styled.div`
  display: flex:
  flex-direction: column;
  flex: 1;
  padding-left: 10px;

  @media (max-width: ${MediaQueryWidths.medium}px) {
    padding-left: 0;
  }
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  &:last-of-type {
    @media (max-width: ${MediaQueryWidths.small}px) {
      display: none;
    }
  }
`;

const Title = styled.span`
  font-weight: 600;

  @media (max-width: ${MediaQueryWidths.small}px) {
    font-size: 12px;
  }
`;

const SubTitle = styled.span`
  color: #c8ced5;
  font-size: 13px;
  font-weight: 300;
`;

const Amount = styled.span`
  font-weight: 400;

  @media (max-width: ${MediaQueryWidths.small}px) {
    font-size: 12px;
  }
`;

const SubAmount = styled.span`
  color: #c8ced5;
`;

export const HeaderDetails = () => {
  const { data: exchangeRates } = useExchangeRates();
  const { data: tokenData } = useTokens();

  const marketCap = useMemo(() => {
    if (!tokenData) return "--";
    const data = tokenData.reduce((a, b) => a + b.marketCap, 0);
    return Number(data.toFixed(2)).toLocaleString("en-US");
  }, [tokenData]);

  return (
    <Wrapper>
      <Item>
        <Label>
          <Image src="/icons/btc-asset-icon.svg" height={40} width={40} />
        </Label>
        <Col>
          <Row>
            <Title>BTC</Title>
            <Amount>
              $
              {exchangeRates
                ? Number(
                    exchangeRates?.BTCL ?? exchangeRates?.PBTC
                  ).toLocaleString("en-US")
                : "--"}
            </Amount>
          </Row>
          <Row>
            <SubTitle>Bitcoin</SubTitle>
          </Row>
        </Col>
      </Item>
      <Item>
        <Label>
          <Image src="/icons/libre-asset-icon.svg" height={40} width={40} />
        </Label>
        <Col>
          <Row>
            <Title>LIBRE</Title>
            <Amount>
              ${exchangeRates ? `${exchangeRates?.LIBRE.toFixed(6)}` : "--"}
            </Amount>
          </Row>
          <Row>
            <SubTitle>Libre</SubTitle>
            <SubTitle>
              {exchangeRates ? (
                <SubAmount>{`${Number(exchangeRates.BTCSAT * 100000000).toFixed(
                  2
                )} SATS`}</SubAmount>
              ) : (
                <></>
              )}
            </SubTitle>
          </Row>
        </Col>
      </Item>
      <Item>
        <Label>
          <IconPairs
            imgSrc1="/icons/libre-asset-icon.svg"
            imgSrc2="/icons/btc-asset-icon.svg"
            imgSrc3="/icons/usdt-asset-icon.png"
            iconSize={40}
          />
        </Label>
        <Col>
          <Row>
            <Title>TVL</Title>
            <Amount>${marketCap ?? "--"}</Amount>
          </Row>
          <Row>
            <SubTitle>Total value locked</SubTitle>
          </Row>
        </Col>
      </Item>
    </Wrapper>
  );
};
