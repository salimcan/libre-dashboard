import styled from "styled-components";

const Title = styled.h1`
  margin: 35px 0 15px 0;
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.titleText};
`;

interface IPageSubtitle {
  title: string;
}

export const PageSubtitle = ({ title }: IPageSubtitle) => {
  return <Title>{title}</Title>;
};
