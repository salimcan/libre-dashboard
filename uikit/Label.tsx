import styled from "styled-components";

const Container = styled.div<{ backgroundColor: string }>`
  width: 80px;
  height: 24px;
  border-radius: 4px;
  background-color: ${(p) => p.backgroundColor};
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Text = styled.span<{ color: string }>`
  font-size: 12px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => p.color};
`;

interface ILabel {
  color: string;
  backgroundColor: string;
  children: string;
}

export const Label = ({ color, backgroundColor, children }: ILabel) => {
  return (
    <Container backgroundColor={backgroundColor}>
      <Text color={color}>{children}</Text>
    </Container>
  );
};
