import Image from "next/image";
import styled, { css } from "styled-components";

const Wrapper = styled.div<{ type: string }>`
  width: 100%;
  display: flex;
  align-items: center;
  padding: 24px;
  border-radius: 8px;

  ${(p) =>
    p.type === "warning" &&
    css`
      background: ${(p) => p.theme.notificationWarningBackground};
    `}
  ${(p) =>
    p.type === "error" &&
    css`
      background: ${(p) => p.theme.errorWarningBackground};
    `}
    ${(p) =>
    p.type === "success" &&
    css`
      background: ${(p) => p.theme.notificationSuccessBackground};
    `}
`;

const IconCol = styled.div`
  display: flex;
  align-items: flex-start;
  flex-shrink: 0;
  padding: 0 24px 0 0;
`;

const Text = styled.span<{ type: string }>`
  font-size: 14px;
  font-weight: 500;
  overflow: hidden;

  ${(p) =>
    p.type === "warning" &&
    css`
      color: ${(p) => p.theme.notificationWarningText};
    `}

  ${(p) =>
    p.type === "error" &&
    css`
      color: ${(p) => p.theme.errorWarningText};
    `}
    ${(p) =>
    p.type === "success" &&
    css`
      color: ${(p) => p.theme.notificationSuccessText};
    `}
`;

const iconPaths = {
  error: "/icons/error-status-icon.svg",
  warning: "/icons/warning-status-icon.svg",
  success: "/icons/success-status-icon-white.svg",
};

interface INotification {
  type: "warning" | "error" | "success";
  text: string;
  onClick?: () => {};
}

export const Notification = ({ type, text, onClick }: INotification) => {
  return (
    <Wrapper type={type}>
      <IconCol>
        <Image src={iconPaths[type]} width={24} height={24} color={"#ffffff"} />
      </IconCol>
      <Text onClick={onClick} type={type}>
        {text}
      </Text>
    </Wrapper>
  );
};
