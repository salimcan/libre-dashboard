import styled, { css } from "styled-components";

const Wrapper = styled.div`
  width: 100%;
`;

const Input = styled.textarea<{ error?: boolean }>`
  width: 100%;
  padding: 14px 26px 14px 16px;
  border-radius: 8px;
  border: 1px solid #c0c0c7;
  background-color: #fff;
  font-size: 16px;
  font-weight: 400;
  line-height: 1.5;
  font-family: Poppins;

  ::placeholder {
    font-size: 16px;
    line-height: 1.5;
    color: #777685;
  }

  ${(p) =>
    p.error &&
    css`
      border: 1px solid ${(p) => p.theme.inputErrorBorder};

      &:focus {
        outline: 1px solid ${(p) => p.theme.inputErrorBorder};
      }
    `}
`;

export const EmptyTextarea = ({ placeholder, value, onChange, error }) => {
  return (
    <Wrapper>
      <Input
        rows={4}
        placeholder={placeholder}
        onChange={onChange}
        value={value}
        error={error}
      />
    </Wrapper>
  );
};
