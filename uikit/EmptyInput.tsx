import styled, { css } from "styled-components";

const Wrapper = styled.div`
  width: 100%;
`;

const Input = styled.input<{
  error?: boolean;
  hasLeft?: boolean;
  hasRight?: boolean;
}>`
  height: 48px;
  width: 100%;
  padding: 14px 26px 14px 16px;
  border-radius: 8px;
  border: solid 1px #c0c0c7;
  background-color: #fff;
  font-size: 16px;
  line-height: 1.5;
  outline: none;
  font-family: Poppins, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
    Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;

  &::placeholder {
    /* Chrome, Firefox, Opera, Safari 10.1+ */
    font-size: 16px;
    line-height: 1.5;
    color: #777685;
    opacity: 1; /* Firefox */
  }

  &:-ms-input-placeholder {
    /* Internet Explorer 10-11 */
    font-size: 16px;
    line-height: 1.5;
    color: #777685;
  }

  &::-ms-input-placeholder {
    /* Microsoft Edge */
    font-size: 16px;
    line-height: 1.5;
    color: #777685;
  }

  ${(p) =>
    p.error &&
    css`
      border: 1px solid ${(p) => p.theme.inputErrorBorder};

      &:focus {
        outline: 1px solid ${(p) => p.theme.inputErrorBorder};
      }
    `}

  ${(p) =>
    p.hasLeft &&
    css`
      padding-left: 80px;
    `}
`;

interface IEmptyInput {
  placeholder: string;
  type?: string;
  value: string | number;
  onChange: (payload: any) => void;
  error?: boolean;
  hasLeft?: boolean;
  hasRight?: boolean;
}

export const EmptyInput = ({
  placeholder,
  type = "text",
  value,
  onChange,
  error,
  hasLeft,
  hasRight,
}: IEmptyInput) => {
  return (
    <Wrapper>
      <Input
        type={type}
        placeholder={placeholder}
        onChange={onChange}
        value={value}
        error={error}
        hasLeft={hasLeft}
        hasRight={hasRight}
      />
    </Wrapper>
  );
};
