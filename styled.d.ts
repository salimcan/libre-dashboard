import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    titleText: string;
    titleGradient: string;
    greyText: string;
    textAccentOrange: string;
    descriptionText: string;

    buttonOrange: string;
    buttonOrangeText: string;
    buttonWhite: string;
    buttonWhiteText: string;
    buttonRed: string;
    buttonRedText: string;
    buttonGrey: string;
    buttonGreyText: string;

    headerBackground: string;
    headerTextColor: string;
    headerTextHover: string;
    headerInactiveColor: string;

    backgroundColor: string;

    leftAnnouncementBackground: string;
    rightAnnouncementBackground: string;

    detailBlockBackground: string;
    detailBlockBorder: string;
    detailBlockTitle: string;
    detailBlockDetails: string;
    detailBlockAccentRed: string;
    detailBlockAccentGreen: string;

    tableHeader: string;

    labelPurpleBackground: string;
    labelPurple: string;
    labelGreenBackground: string;
    labelGreen: string;
    labelOrangeBackground: string;
    labelOrange: string;
    labelGreyBackground: string;
    labelGrey: string;
    labelRedBackground: string;
    labelRed: string;

    inputBorder: string;
    inputErrorBorder: string;

    notificationWarningBackground: string;
    notificationWarningText: string;
    notificationSuccessBackground: string;
    notificationSuccessText: string;
    errorWarningBackground: string;
    errorWarningText: string;

    progressBarBackground: string;
    progressBarGreen: string;
    progressBarRed: string;

    sliderBarBackground: string;
    sliderBarInactive: string;
  }
}
